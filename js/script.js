"use strict";

function checkPhoneNumber(phoneNumber) {
    const patternMatching = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){11}(\s*)?$/.exec(`${phoneNumber}`);
    if (patternMatching == null) {
        return false;
    }
    return true;
}

function checkFullName(fullName) {
    const patternMatching = /([А-Я]{1}[а-я]{1,50})$/.exec(`${fullName}`);

    if (patternMatching == null) {
        return false;
    }
    return true;
}

function checkEmail(email) {
    const patternMatching = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.exec(email);

    if (patternMatching == null) {
        if (email != ''){
            return false;
        }
    }
    return true;
}

async function postMessage(fullName, phoneNumber, email) {
    const url = 'https://60376bfd5435040017722533.mockapi.io/form';

    const data = {
        name: fullName,
        phone: phoneNumber,
        email: email
    };

    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    });

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        });
        const json = await response.json();
        console.log('Успех:', JSON.stringify(json));
        alert(`Вернулся ${response.status} ответ`);
    } catch (error) {
        console.error('Ошибка:', error);
        alert(`Вернулся ${response.status} ответ`);
    }
}

const addData = document.querySelector('.registerbtn');
const button1 = document.getElementById('button1');

addData.addEventListener('click', (event) => {
    event.preventDefault();

    const fullName = document.getElementById('fullname').value,
        phoneNumber = document.getElementById('phone').value,
        email = document.getElementById("email").value;

    let faileString = '';

    if (!checkFullName(fullName)) {
        faileString += "Неверно введён ФИО\n";
    }

    if (!checkPhoneNumber(phoneNumber)) {
        faileString += "Неверно введён номер телефона\n";
    }

    if (!checkEmail(email)) {
        faileString += "Неверно введён email";
    }

    if (faileString != '') {
        alert(faileString);
    } else {
        postMessage(fullName, phoneNumber, email);
    }
});